<?php
    $multiple1 = 3;
    $multiple2 = 5;

    for ($i = 1; $i <= 100; $i++) {

        $data = "";

        if($i%$multiple1 == 0){
            $data = "Fizz";
        }

        if($i%$multiple2 == 0){
            $data = "Buzz";
        }

        if($i%$multiple1 == 0 && $i%$multiple2 == 0){
            $data = "FizzBuzz";
        }

        if($i%$multiple1 != 0 && $i%$multiple2 != 0){
            $data = $i;
        }

        echo $data;
        echo "<br>";
    }