<?php

    // 1 - construction of the array for to create matriz
    $arrayNumbers = array();

    for ($i = 1; $i <= 500; $i++) {
        array_push($arrayNumbers, $i);
    }


    // 2 - construction of matriz
    $arrayMatriz = array();

    do{

        //to get random number
        $random = array_rand($arrayNumbers);

        //To check and add value into array if not exist it
        if(!in_array($random, $arrayMatriz)){
            array_push($arrayMatriz, $random);
        }

    }while(sizeof($arrayMatriz) < sizeof($arrayNumbers));


    // 3 - delete random position of matriz
    $deleteRandom = array_rand($arrayMatriz);

    //I make sure not to access a non-existent value in array
    $deleteRandom = $deleteRandom - 1;
    $valueDeleted = $arrayMatriz[$deleteRandom];
    $arrayMatriz[$deleteRandom] = "";

    //Show by screen the value stored in the randomly deleted position of the array
    var_dump($valueDeleted);