<?php

    require("ModelUser.php");

    $modelUser = new ModelUser();

    //Test get all user
    $modelUser->getAllUser();

    //Test create new user
    $user = new User("Víctor Gracia Magallón", 28, "Backend Developer");
    $modelUser->createUser($user);