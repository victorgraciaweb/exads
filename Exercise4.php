<?php

    require("NationalLotery.php");

    $nationalLotery = new NationalLotery();

    //Test Get date national lotery today
    try {
        $today = new DateTime("now");
    } catch (Exception $e) {
        echo $e;
    }
    $nationalLotery->GetDate($today);

    //Test Get date national lotery (with optional date)
    try {
        $dateUser = new DateTime("2019-02-14 18:15:26");
    } catch (Exception $e) {
        echo $e;
    }
    $nationalLotery->GetDate($dateUser);

