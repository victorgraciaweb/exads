<?php
    require("Connection.php");
    require("User.php");

    class ModelUser{

        private $newConn;

        public function __construct(){
            // I create new instance
            $newConn = new Connection();
            $this->newConn = $newConn;
        }

        public function getAllUser(){
            // New connection
            $this->newConn->CreateConnection();

            $query = "SELECT name, age, job_title FROM exads_test";
            $result = $this->newConn->ExecuteQuery($query);
            if ($result) {

                while ($row = $this->newConn->GetRows($result)) {
                    echo "USER:" . $row[1] . " " . $row[2] . " " . $row[3];
                }

                $this->newConn->SetFreeResult($result);

            } else {
                echo "<h3>Error</h3>";
            }

            // I close connection DB
            $this->newConn->CloseConnection();
        }

        public function createUser($user){
            // New connection
            $this->newConn->CreateConnection();

            $query = "INSERT INTO exads_test (name, age, job_title) VALUES ('".$user->getName()."', '".$user->getAge()."', '".$user->getAge()."')";
            $result = $this->newConn->ExecuteQuery($query);
            if ($result) {
                $RowCount = $this->newConn->GetCountAffectedRows();
                if ($RowCount > 0) {
                    echo "Ok<BR>";
                }
            } else {
                echo "<h3>Error</h3>";
            }

            // I close connection DB
            $this->newConn->CloseConnection();
        }
    }