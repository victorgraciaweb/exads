<?php

class NationalLotery {
    public function GetDate($date){

        $nextWednesday = date("Y-m-d H:i:s", strtotime("next Wednesday"));
        $nextSaturday = date("Y-m-d H:i:s", strtotime("next Saturday"));

        try {
            $dateNextWednesday = new DateTime($nextWednesday);
            $dateNextSaturday = new DateTime($nextSaturday);

            $dateNextWednesday->modify('+20 hours');
            $loteryWednesday = new DateTime($dateNextWednesday->format('d-m-Y H:i:s'));

            $dateNextSaturday->modify('+20 hours');
            $loterySaturday = new DateTime($dateNextSaturday->format('d-m-Y H:i:s'));
        } catch (Exception $e) {
            echo $e;
        }

        if($date > $loterySaturday){
            $nextDate = $dateNextWednesday->format('d-m-Y H:i:s');
        }else{
            $nextDate = $dateNextSaturday->format('d-m-Y H:i:s');
        }

        return $nextDate;
    }
}