<?php

    class User {
        private $name;
        private $age;
        private $jobTitle;

        function __construct($name, $age, $jobTitle) {
            $this->name = $name;
            $this->age = $age;
            $this->jobTitle = $jobTitle;
        }

        public function getName(){
            return $this->name;
        }

        public function getAge(){
            return $this->age;
        }

        public function getJobTitle(){
            return $this->jobTitle;
        }
    }
